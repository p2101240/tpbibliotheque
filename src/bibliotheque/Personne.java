package bibliotheque;

public class Personne {
    private int numeroPers;
    private String nomPers;
    private String prenomPers;
    private int anNaissance;
    private int dernierNumero;
    
    public Personne(String nom,String prenom,int anneeNaissance)
    {
        this.nomPers = nom;
        this.prenomPers = prenom;
        this.anNaissance = anneeNaissance;
    }
    
    public void setNumPers(int numero)
    {
        this.numeroPers = numero;
    }
    
    public int getDernierNum()
    {
        return dernierNumero;
    }
    
    public int getNumero()
    {
        return numeroPers;
    }
    
    public String getNom()
    {
        return nomPers;
    }
    
    public String getPrenom()
    {
        return prenomPers;
    }
    
    public int getAnNaissance()
    {
        return anNaissance;
    }
    
    public void setNomPers(String nom)
    {
        this.nomPers = nom;
    }
    
    public void setPrenomPers(String prenom)
    {
        this.prenomPers = prenom;
    }
    
    public void setAnNaissance(int annee)
    {
        this.anNaissance = annee;
    }
    
    @Override
    public String toString()
    {
        return numeroPers + " " + nomPers + " " + prenomPers + " " + anNaissance + " " + dernierNumero;
    }
}
